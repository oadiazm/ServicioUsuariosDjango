FROM python:3
LABEL maintainer="info@oscarandresdiaz.com"
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /opt/app-root/src
WORKDIR /opt/app-root/src
ADD requirements.txt /opt/app-root/src/
RUN pip install -r requirements.txt
ADD ./src /opt/app-root/src/
