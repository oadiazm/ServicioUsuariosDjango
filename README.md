Servicio de Usuarios Python/Django/PostgreSQL con Docker
========================================================

## Descripción

Se necesita implementar un servicio que provea una interfaz HTTP para conocer información de sus usuarios, dicha información es sensible y debe tener poseer al menos seguridad básica HTTP. Este servicio de usuarios será consumido por otro servicio interno ue necesita obtener todos los datos de los usuarios para hacer envio de emails personalizados, por lo cual deberá retornar una serie de datos en formato JSON para su fácil consumo.<br/>

Nuestros desarrolladores se encuentran trabajando con un servicio de emails y poseen un contrato entre los sistemas.
Para el request de información de usuarios el sistema de emails hará una petición GET al endpoint /users enviando en el HEADER una API KEY de 40 caracteres.<br/>

Para el retorno de información de usuarios, el sistema de usuarios debe retornar un código HTTP 200 a la petición GET con un array de usuarios, ej: <br/>

`[
  {
    "username" : "example1" ,
    "email" : "example1@example.com" ,
    "created_at" : "2015-04-0122:07:11.038872" ,
    "last_login" : "2015-04-1510:02:16.489345" ,
  },
  {
    "username" : "example2" ,
    "email" : "example2@example.com" ,
    "created_at" : "2015-04-0122:08:11.038872" ,
    "last_login" : "2015-04-1510:03:16.489345" ,
  }
]`

En caso que la API KEY sea incorrecta debe retornar un código HTTP 401 a la peticion GET con un body JSON como el siguiente: <br/>

`{
  "status" : "APIKeyinvalid"
}`

La información del servicio de usuarios estara en un archivo CSV con aproximadamente 1000 registros, se debe usar PostgreSQL para el almacenamiento de datos y Python/Django para la interfaz/servicio.

## Objetivos

* Migrar la data del CSV a la base de datos (Crear archivo de migración, si es necesario).
* Crear un servicio web con la seguridad correspondiente que devuelva la información de los usuarios.
* Compartir en un repo Git el resultado con un README.md con las instrucciones de instalación y puesta en marcha del servicio de usuario.

## Instalación

### Docker

Ver instrucciones de instalación en: [Documentación de Docker](https://docs.docker.com/)

### Docker Compose

Instalar Docker Componer, ver instrucciones de instalación en: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

#### Postman (Opcional)

Para las pruebas puede usar: [Postman](https://www.getpostman.com)

### Configuración
* Clonar el repositorio <br/> `git clone git@gitlab.com:oadiazm/ServicioUsuariosDjango.git` <br/> `cd ServicioUsuariosDjango`.
* Realizar la migración de la base de datos: <br/> `docker-compose run web ./manage.py migrate`.
* Ejecutar: <br/> `docker-compose run web ./manage.py collectstatic -v 0 --noinput`.
* Crear un super usuario: <br/> `docker-compose run web ./manage.py createsuperuser`.
* Correr: `docker-compose up`.
* Abrir el navegador e ingresar con las credenciales del usuario creado: [http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/).
* Importar la data del archivo `test_data.csv` a la base de datos en: [http://127.0.0.1:8000/admin/auth/user/import/](http://127.0.0.1:8000/admin/auth/user/import/).
* Probar el endpoint /users enviando en el HEADER una API KEY de 40 caracteres: `APIKEY: 1234567890123456789012345678901234567890` con autorización básica `http://127.0.0.1:8000/users/`.
* Ingresar al contenedor: `docker exec -it djangoapp bash`.
