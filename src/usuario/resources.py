from django.contrib.auth.models import User
from import_export.fields import Field
from import_export.widgets import DateTimeWidget
from import_export.resources import ModelResource


class UserResource(ModelResource):

    widget = DateTimeWidget(format='%Y-%m-%d %H:%M:%S.%f')
    created_at = Field(attribute='date_joined', widget=widget)
    last_login = Field(attribute='last_login', widget=widget)

    class Meta:
        model = User
        import_id_fields = ['username']
        fields = ('username', 'email', 'created_at', 'last_login',)
        export_order = ('username', 'email', 'created_at', 'last_login',)
