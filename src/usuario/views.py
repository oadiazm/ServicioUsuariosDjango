from django.contrib.auth.models import User
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated
from .serializers import UserSerializer
from .permissions import KeyAuthentication


class ReadOnlyUserListViewSet(ReadOnlyModelViewSet):
    """
    View to list all users in the system.

    * Requires key authentication.
    * Only authenticated users are able to access this view.
    """
    authentication_classes = (KeyAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer
    http_method_names = (u'get')
    queryset = User.objects.all().order_by('-date_joined')
