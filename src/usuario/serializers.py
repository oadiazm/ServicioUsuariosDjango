from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    formato = '%Y-%m-%d %H:%M:%S.%f'
    created_at = serializers.DateTimeField(
        source='date_joined', format=formato)
    last_login = serializers.DateTimeField(format=formato)

    class Meta:
        model = User
        fields = ('username', 'email', 'created_at', 'last_login',)
