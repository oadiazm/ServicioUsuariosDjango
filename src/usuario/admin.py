from django.contrib import admin
from django.contrib.auth.models import User
from import_export.admin import ImportExportModelAdmin
from .resources import UserResource


class UserAdmin(ImportExportModelAdmin):
    resource_class = UserResource


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
