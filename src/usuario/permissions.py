import os
from rest_framework import authentication
from rest_framework import exceptions, HTTP_HEADER_ENCODING


def get_key_authentication_header(request):
    key = request.META.get('HTTP_APIKEY', b'')
    if isinstance(key, type('')):
        key = key.encode(HTTP_HEADER_ENCODING)
    return key


class KeyAuthentication(authentication.BasicAuthentication):

    def authenticate(self, request):
        try:
            APIKEY = str.encode(os.environ['API_KEY'])
            key = get_key_authentication_header(request).split()
            if not key or key[0].lower() != APIKEY:
                raise exceptions.AuthenticationFailed()
        except Exception:
            raise exceptions.AuthenticationFailed({'status': 'APIKeyinvalid'})
        return super(KeyAuthentication, self).authenticate(request)
